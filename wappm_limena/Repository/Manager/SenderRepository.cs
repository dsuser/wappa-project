﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wappm_limena.Models;
using wappm_limena.Repository.IFaces;

namespace wappm_limena.Repository.Manager
{
    public class SenderRepository : ISender
    {
        private readonly dbLimenaEntities _limenaContex;
        private readonly DLI_PROEntities _sapContext;
        public SenderRepository(dbLimenaEntities limenaContex, DLI_PROEntities sapContext)
        {
            _limenaContex = limenaContex;
            _sapContext = sapContext;
        }

        public List<Rpt_UserSend> GetSenders()
        {
            return _limenaContex.Rpt_UserSend.ToList();
        }

        public List<view_VendorsData> GetVendorsData(Rpt_UserSend vendor)
        {
            return (from b in _sapContext.view_VendorsData where (b.CODIGO_CLIENTE == vendor.VendorID) select b).ToList();
        }
    }
}