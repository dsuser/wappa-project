﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using wappm_limena.Models;
using wappm_limena.Repository.IFaces;

namespace wappm_limena.Repository.Manager
{
    public class UOWManager : IUOW
    {
        public ISender senderRepository { get; }

        public UOWManager(dbLimenaEntities limenaContex, DLI_PROEntities sapContext)
        {
            senderRepository = new SenderRepository(limenaContex, sapContext);
        }
    }
}