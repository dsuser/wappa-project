﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wappm_limena.Models;

namespace wappm_limena.Repository.IFaces
{
    public interface ISender
    {
        List<Rpt_UserSend> GetSenders();
        List<view_VendorsData> GetVendorsData(Rpt_UserSend vendor);
    }
}
