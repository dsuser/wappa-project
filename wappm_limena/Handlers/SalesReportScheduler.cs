﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using wappm_limena.Jobs;

namespace wappm_limena.Handlers
{
    public class SalesReportScheduler
    {
        public static async Task Start()
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder
                .Create<SalesJob>()
                .WithIdentity("Sales", "Reports")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("ReportsTrigger", "Reports")
                //.WithSimpleSchedule(x => x
                //.WithIntervalInMinutes(5)
                //.RepeatForever())
                .WithSchedule(CronScheduleBuilder.WeeklyOnDayAndHourAndMinute(DayOfWeek.Tuesday, 07, 00))
                .Build();
            await scheduler.ScheduleJob(job, trigger);

        }
    }
}