﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using wappm_limena.Jobs;

namespace wappm_limena.Handlers
{
    public class AccountsReceivableScheduler
    {
        public static async Task Start()
        {
            StdSchedulerFactory factory = new StdSchedulerFactory();
            IScheduler scheduler = await factory.GetScheduler();
            await scheduler.Start();

            IJobDetail job = JobBuilder
                .Create<AccountsJob>()
                .WithIdentity("Accounts", "Receivable")
                .Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("AccountsTrigger", "Receivable")
                //.WithSimpleSchedule(x => x
                //.WithIntervalInMinutes(5)
                //.RepeatForever())
                .WithSchedule(CronScheduleBuilder.WeeklyOnDayAndHourAndMinute(DayOfWeek.Tuesday, 06, 00))
                .Build();
            await scheduler.ScheduleJob(job, trigger);

        }
    }
}