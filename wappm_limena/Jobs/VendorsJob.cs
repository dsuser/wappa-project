﻿using ClosedXML.Excel;
using Quartz;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using wappm_limena.Helper;
using wappm_limena.Models;
using wappm_limena.Repository.IFaces;
using wappm_limena.Repository.Manager;

namespace wappm_limena.Jobs
{
    public class VendorsJob : IJob
    {
        private readonly IUOW uOW;
        public VendorsJob()
        {
            this.uOW = new UOWManager(new dbLimenaEntities(), new DLI_PROEntities());
        }
        public async Task Execute(IJobExecutionContext context)
        {

            var date = DateTime.Now.ToString("yyyyMMdd");
            string[] Configuration = WebConfigurationManager.AppSettings["EmailConfiguration"].Split(';');
            List<Rpt_UserSend> Senders = uOW.senderRepository.GetSenders();
            List<Rpt_UserSend> Vendordata = Senders.Where(rs => rs.Active == true && rs.VendorReport == true && rs.IsSeller == true && !String.IsNullOrEmpty(rs.VendorID)).ToList();
            List<Rpt_UserSend> CcData = Senders.Where(rs => rs.Active == true && rs.VendorReport == true && rs.IsSeller == false).ToList();

            if (Vendordata.Count > 0)
            {
                foreach (var vendor in Vendordata)
                {
                    List<view_VendorsData> lista_datos = new List<view_VendorsData>();
                    lista_datos = uOW.senderRepository.GetVendorsData(vendor);
                    if (lista_datos.Count() >= 0)
                    {
                        try
                        {
                            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(view_VendorsData));
                            DataTable table = new DataTable();
                            for (int i = 0; i < props.Count - 3; i++)
                            {
                                PropertyDescriptor prop = props[i];
                                table.Columns.Add(prop.Name, prop.PropertyType);
                            }
                            object[] values = new object[props.Count - 3];
                            foreach (var item in lista_datos)
                            {
                                for (int i = 0; i < values.Length; i++)
                                {
                                    values[i] = props[i].GetValue(item);
                                }
                                table.Rows.Add(values);
                            }
                            DataTable dt = table;
                            decimal n = DateTime.Now.DayOfYear;
                            decimal f = Math.Ceiling(n / 7);
                            int weekNum = Convert.ToInt32(f) - 1;
                            int LimenaWeek = weekNum + 9;
                            var filePathOriginal = HostingEnvironment.MapPath("~/SharedContent/Reports/excel");
                            var name = vendor.VendorName + "_DATA_WEEK_" + LimenaWeek + ".xlsx";
                            DataSet ds = GetDataSet(table);

                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                wb.Worksheets.Add(ds);
                                wb.Worksheet(1).Name = "EDT_CORPORATION_DATA";
                                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                                wb.Style.Font.Bold = true;
                                var path2 = "";
                                path2 = Path.Combine(filePathOriginal, name);
                                wb.SaveAs(path2);
                            }
                            var pathforemail = Path.Combine(filePathOriginal, name);

                            try
                            {

                                var todayLastWeek = DateTime.Today.AddDays(-7);
                                var sunday = todayLastWeek.AddDays(-(int)DateTime.Today.DayOfWeek);
                                var saturday = sunday.AddDays(6);
                                MailMessage objeto_mail = new MailMessage();
                                SmtpClient client = new SmtpClient();
                                client.Port = 587;

                                client.Host = "smtp-mail.outlook.com";
                                client.Timeout = 100000;
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.UseDefaultCredentials = false;
                                client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                                objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                                objeto_mail.To.Add(new MailAddress(vendor.Email.ToString())); //Comentar en pruebas
                                //objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net")); //Descomentar si se haran pruebas
                                objeto_mail.Subject = vendor.VendorName + " DATA REPORT | " + sunday.ToShortDateString() + " - " + saturday.ToShortDateString();
                                objeto_mail.Attachments.Add(new Attachment(pathforemail));
                                foreach (var bcc in CcData)
                                {
                                    //Comentar esto en caso de pruebas
                                    MailAddress bccEmail = new MailAddress(bcc.Email.ToString());
                                    objeto_mail.CC.Add(bccEmail);
                                }
                                client.Send(objeto_mail);
                            }
                            catch (Exception ex)
                            {
                                MailMessage objeto_mail = new MailMessage();
                                SmtpClient client = new SmtpClient();
                                client.Port = 587;
                                client.Host = "smtp-mail.outlook.com";
                                client.Timeout = 100000;
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.UseDefaultCredentials = false;
                                client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                                objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                                objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net"));
                                objeto_mail.Subject = "Ha ocurrido un error..";
                                objeto_mail.Body = ex.Message;
                                client.Send(objeto_mail);

                            }

                        }
                        catch (Exception ex)
                        {
                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                            objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                            objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net"));
                            objeto_mail.Subject = "Ha ocurrido un error..";
                            objeto_mail.Body = ex.Message;
                            client.Send(objeto_mail);
                        }
                    }
                    else
                    {
                        //Realizamos solicitud de datos
                        //Console.WriteLine("No data was found for " + vendor.VendorName);
                    }
                }
            }

            else
            {
                //Console.WriteLine("No Vendor List Data was found...");
                //Console.WriteLine("Exit program...");
            }
            await Console.Out.WriteLineAsync("Vendors Job");
        }

        public DataSet GetDataSet(DataTable table)
        {
            DataSet ds = new DataSet("EDT_CORPORATION_DATA");
            ds.Tables.Add(table);
            return ds;
        }
    }

}