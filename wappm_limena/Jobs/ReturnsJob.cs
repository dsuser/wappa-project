﻿using CrystalDecisions.CrystalReports.Engine;
using Quartz;
using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using wappm_limena.Models;

namespace wappm_limena.Jobs
{
    public class ReturnsJob : IJob
    {
        private DLI_PROEntities db = new DLI_PROEntities();
        private dbLimenaEntities dblim = new dbLimenaEntities();
        public async Task Execute(IJobExecutionContext context)
        {
            var date = DateTime.Now.ToString("yyyyMMdd");
            string[] Configuration = WebConfigurationManager.AppSettings["EmailConfiguration"].Split(';');

            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            ostrm = new FileStream(HostingEnvironment.MapPath("/logs/ralog_" + date + ".txt"), FileMode.OpenOrCreate, FileAccess.Write);
            writer = new StreamWriter(ostrm);

            Console.SetOut(writer);

            var path = "";
            var pathimage = "";
         
            Console.WriteLine("Auto Mail Sender for Return and Allowances");
            Console.WriteLine("Returning Sellers list...");
            var Sellersdata = dblim.Sys_Users.Where(c => c.Roles.Contains("Sales Representative") && c.Active).ToList(); //readXML.ReturnListOfSellersAR_console(); 
            Console.WriteLine("Returning CC list...");

           // var CcData = readXML.ReturnListOfCc_console();
            List<Rpt_UserSend> Senders = dblim.Rpt_UserSend.ToList();
            List<Rpt_UserSend> CcData = Senders.Where(rs => rs.Active == true && rs.ReturnReport == true).ToList();
            //var destinatariosCC = "";
            //int count = 1;


            if (Sellersdata.Count > 0)
            {
                Console.WriteLine("Sending emails...");
                foreach (var seller in Sellersdata)
                {
                    int idsap = Convert.ToInt32(seller.IDSAP);
                    var returns = (from c in db.BI_Email_RA where (c.id_SalesRep == idsap) select c).ToList();
                    if (returns.Count() > 0)
                    {

                        //Existen datos
                        //Buscamos para tabla reason
                        var returns_header = (from b in db.BI_Email_RA_Head where (b.SlpCode == idsap) select b).OrderByDescending(b => b.Descr == "EXPIRED W99").ThenByDescending(b => b.Returns).ToList();

                        ReportDocument rd = new ReportDocument();
                        rd.Load(Path.Combine(HostingEnvironment.MapPath("~/Reports"), "rptReturnsAndAllowancesBySeller.rpt"));

                        rd.SetDataSource(returns);
                        rd.Subreports[0].SetDataSource(returns_header);

                        string fecha = DateTime.Now.ToLongDateString();
                        rd.SetParameterValue("fecha_actual", fecha);

                        rd.SetParameterValue("nombre", seller.Name.ToUpper());
                        rd.SetParameterValue("apellido", seller.Lastname.ToUpper());
                        var totalBudget = returns_header.AsEnumerable().Sum(x => x.Budget);
                        rd.SetParameterValue("budget", totalBudget);
                        var totalReturns = returns.Where(x => x.Budget > 0).AsEnumerable().Sum(x => x.Returns);
                        rd.SetParameterValue("returns", totalReturns);


                        if (totalBudget > 0)
                        {
                            var totalAchievements = (totalReturns / totalBudget) * 100;
                            rd.SetParameterValue("total_achievements", totalAchievements);
                        }
                        else
                        {
                            var totalAchievements = 0.00;
                            rd.SetParameterValue("total_achievements", totalAchievements);
                        }

                        var filePathOriginal = new Uri(
                        System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                        ).LocalPath;//Path of the xml script  

                        filePathOriginal = filePathOriginal + "\\Reports\\pdfReports";

                        try
                        {

                            //limpiamos el directorio
                            System.IO.DirectoryInfo di = new DirectoryInfo(filePathOriginal);

                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }

                        }
                        catch (Exception e)
                        {
                            var mensaje = e.ToString();
                        }



                        var filename = "Return and Allowances " + seller.Name + " " + seller.Lastname + ".pdf";
                        path = Path.Combine(filePathOriginal, filename);
                        rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);

                        PdfDocument doc = new PdfDocument();
                        doc.LoadFromFile(path);
                        Image img = doc.SaveAsImage(0);
                        var imagename = "Return and Allowances " + seller.Name + " " + seller.Lastname + ".jpg";
                        pathimage = Path.Combine(filePathOriginal, imagename);
                        img.Save(pathimage);
                        doc.Close();


                        //Para enviar correos
                        try
                        {

                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(Configuration[0], Configuration[1]);

                            objeto_mail.IsBodyHtml = true;
                            objeto_mail.AlternateViews.Add(getEmbeddedImage(pathimage));


                            objeto_mail.From = new MailAddress(Configuration[0]);
                            objeto_mail.To.Add(new MailAddress(seller.Email.ToString()));
                            objeto_mail.Subject = "Returns and Allowances Report | " + seller.Name + " " + seller.Lastname + " " + DateTime.Now.ToString("dddd").ToUpper();
                            foreach (var bcc in CcData)
                            {
                                MailAddress bccEmail = new MailAddress(bcc.Email.ToString());
                                objeto_mail.CC.Add(bccEmail);
                            }
                            //MailAddress cc = new MailAddress(seller.Supervisor.ToString());
                            //objeto_mail.CC.Add(cc);

                            //Enviamos el mensaje
                            client.Send(objeto_mail);


                            Console.WriteLine("Email sent successfully to : " + seller.Name.ToString() + " " + seller.Lastname + " - " + seller.Email.ToString());
                        }

                        catch (Exception e)
                        {
                            Console.WriteLine("Email error, can't sent to : " + seller.Name.ToString() + " " + seller.Lastname + " - " + seller.Email.ToString() + ". Error: " + e);
                        }


                    }
                    else
                    {
                        Console.WriteLine("No Data was found for " + seller.Name.ToString() + " " + seller.Lastname);
                    }
                }

            }
            else
            {
                Console.WriteLine("No Data was found...");
                Console.WriteLine("Exit program...");
            }

            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            await Console.Out.WriteLineAsync("Returns Job");
           
        }
        private AlternateView getEmbeddedImage(String filePath)
        {
            LinkedResource res = new LinkedResource(filePath, MediaTypeNames.Image.Jpeg);
            res.ContentId = Guid.NewGuid().ToString();

            string htmlBody = @"<img src='cid:" + res.ContentId + @"'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

            alternateView.LinkedResources.Add(res);
            return alternateView;
        }
    }
}