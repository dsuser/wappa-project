﻿using CrystalDecisions.CrystalReports.Engine;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using wappm_limena.Helper;
using wappm_limena.Models;

namespace wappm_limena.Jobs
{
    public class AccountsJob : IJob
    {

        private DLI_PROEntities db = new DLI_PROEntities();
        private dbLimenaEntities dblim = new dbLimenaEntities();
        public async Task Execute(IJobExecutionContext context)
        {
            string[] Configuration = WebConfigurationManager.AppSettings["EmailConfiguration"].Split(';');
            var date = DateTime.Now.ToString("yyyyMMdd");

            var path = "";
            
            List<Sys_Users> Sellersdata = dblim.Sys_Users.Where(c => c.Roles.Contains("Sales Representative") && c.Active).ToList();
            //List<Sys_Users> Sellersdata = new List<Sys_Users>();
            //Sys_Users test = new Sys_Users();
            //test.IDSAP = "3";
            //test.Name = "dm";
            //test.Lastname = "cs";
            //test.Email = "d.serbino@limenainc.net";
            //Sellersdata.Add(test);

            //Copias del MSG
           // Console.WriteLine("Returning CC list...");
            List<Rpt_UserSend> CcData = dblim.Rpt_UserSend.Where(rs => rs.Active == true && rs.AccountReport == true).ToList();

            if (Sellersdata.Count > 0)
            {
                foreach (var seller in Sellersdata)
                {

                    int idsap = Convert.ToInt32(seller.IDSAP);
                    var accounts_receivable = (from c in db.BI_Accounts_receivable where (c.SalesRepCode == idsap) select c).OrderByDescending(x => x.idAging).ThenByDescending(x => x.Amount).ToList();
                    if (accounts_receivable.Count() > 0)
                    {
                        //Existen datos
                        ReportDocument rd = new ReportDocument();

                        rd.Load(Path.Combine(HostingEnvironment.MapPath("~/Reports"), "rptAccountsReceivableBySeller.rpt"));

                       // rd.Load(reportpath);
                        rd.SetDataSource(accounts_receivable);
                        string fecha = DateTime.Now.ToLongDateString();
                        rd.SetParameterValue("fecha_actual", fecha);
                        rd.SetParameterValue("nombre", seller.Name.ToUpper());
                        rd.SetParameterValue("apellido", seller.Lastname.ToUpper());
                        var totalAR = accounts_receivable.AsEnumerable().Sum(x => x.Amount);
                        rd.SetParameterValue("totalAR", totalAR);
                        var NowDate = DateTime.Now;
                       
                        var filePathOriginal = HostingEnvironment.MapPath("~/SharedContent/Reports/pdf");

                        var filename = "Accounts Receivable Report " + seller.Name + " " + seller.Lastname + "-" + NowDate.Day + "-"+ NowDate.Month + "-" + NowDate.Year + ".pdf";
                        path = Path.Combine(filePathOriginal, filename);
                        try
                        {
                            rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);
                        }
                        catch (Exception ex)
                        {
                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                            objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                            objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net"));
                            objeto_mail.Subject = "Ha ocurrido un error..";
                            objeto_mail.Body = ex.Message;
                            client.Send(objeto_mail);
                        }
                        

                        //Para enviar correos
                        try
                        {
                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(Configuration[0], Configuration[1]);

                            objeto_mail.IsBodyHtml = true;
                            Attachment data = new Attachment(path, MediaTypeNames.Application.Pdf);
                            objeto_mail.Attachments.Add(data);


                            objeto_mail.From = new MailAddress(Configuration[0]);
                            objeto_mail.To.Add(new MailAddress(seller.Email.ToString()));
                            objeto_mail.Subject = "Accounts Receivable Report | " + seller.Name + " " + seller.Lastname;
                            foreach (var bcc in CcData)
                            {
                                MailAddress bccEmail = new MailAddress(bcc.Email.ToString());
                                objeto_mail.CC.Add(bccEmail);
                            }
                            MailAddress cc = new MailAddress(seller.Email.ToString());
                            objeto_mail.CC.Add(cc);

                            //Enviamos el mensaje
                            client.Send(objeto_mail);
                        }

                        catch (Exception ex)
                        {
                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                            objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                            objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net"));
                            objeto_mail.Subject = "Ha ocurrido un error..";
                            objeto_mail.Body = ex.Message;
                            client.Send(objeto_mail);
                        }
                    }
                    else
                    {
                        MailMessage objeto_mail = new MailMessage();
                        SmtpClient client = new SmtpClient();
                        client.Port = 587;
                        client.Host = "smtp-mail.outlook.com";
                        client.Timeout = 100000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new System.Net.NetworkCredential("donotreply@limenainc.net", "Hup65946");
                        objeto_mail.From = new MailAddress("donotreply@limenainc.net");
                        objeto_mail.To.Add(new MailAddress("d.serbino@limenainc.net"));
                        objeto_mail.Subject = "Notificacion";
                        objeto_mail.Body = "No se encontraron datos para el vendedor";
                        client.Send(objeto_mail);
                    }
                }

            }
            else
            {
                //Console.WriteLine("No Data was found...");
                //Console.WriteLine("Exit program...");
            }
            await Console.Out.WriteLineAsync("Accounts Job");
        }
    }
    
}