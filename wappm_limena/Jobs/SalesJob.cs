﻿using CrystalDecisions.CrystalReports.Engine;
using Quartz;
using Spire.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using wappm_limena.Helper;
using wappm_limena.Models;

namespace wappm_limena.Jobs
{
    public class SalesJob : IJob
    {
        private DLI_PROEntities db = new DLI_PROEntities();
        private dbLimenaEntities dblim = new dbLimenaEntities();
        public async Task Execute(IJobExecutionContext context)
        {
            var date = DateTime.Now.ToString("yyyyMMdd");
            string[] Configuration = WebConfigurationManager.AppSettings["EmailConfiguration"].Split(';');

            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;

            ostrm = new FileStream(HostingEnvironment.MapPath("/logs/log_" + date + ".txt"), FileMode.OpenOrCreate, FileAccess.Write);
            writer = new StreamWriter(ostrm);

           // Console.SetOut(writer);

            var path = "";
            var pathimage = "";
            XMLReader readXML = new XMLReader();
            Console.WriteLine("Auto Mail Sender for Sales Report");
            Console.WriteLine("Returning Sellers list...");
            //Vendedores desde azure mas automatizado 10/20/2020

            List<Sys_Users> lstSalesReps = new List<Sys_Users>();
            lstSalesReps = (from b in dblim.Sys_Users where (b.Active == true && b.Departments == "Sales" && b.Roles == "Sales Representative") select b).ToList();

            List<Rpt_UserSend> Senders = dblim.Rpt_UserSend.ToList();

            Console.WriteLine("Returning CC list...");
            
            List<Rpt_UserSend> CcData = Senders.Where(rs => rs.Active == true && rs.SalesReport == true && rs.IsSeller == false).ToList();

            Console.WriteLine("Getting email configuration...");


            if (lstSalesReps.Count > 0)
            {
                Console.WriteLine("Sending emails...");
                foreach (var seller in lstSalesReps)
                {
                    string reportpath = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;//Path of the xml script  

                    var intIDSAP = Convert.ToInt32(seller.IDSAP);
                    var salesorders = (from c in db.BI_Sales_Report where (c.SlpCode == intIDSAP) select c).OrderBy(x => x.Time).ToList();
                    if (salesorders.Count() > 0)
                    {
                        //Existen datos

                        ReportDocument rd = new ReportDocument();

                        reportpath = reportpath + "\\Reports\\rptSalesReportBySeller.rpt";
                        rd.Load(reportpath);

                        rd.SetDataSource(salesorders);
                        string fecha = DateTime.Now.ToLongDateString();
                        rd.SetParameterValue("fecha_actual", fecha);

                        rd.SetParameterValue("nombre", seller.Name.ToUpper());
                        rd.SetParameterValue("apellido", seller.Lastname.ToUpper());
                        rd.SetParameterValue("weektype", "Week" + " " + salesorders.Where(x => x.WeekType != null).FirstOrDefault().WeekType.ToString());

                        var totalBudget = salesorders.AsEnumerable().Sum(x => x.Budget);
                        rd.SetParameterValue("budget", totalBudget);
                        var totalOrderSales = salesorders.Where(x => x.Budget > 0).AsEnumerable().Sum(x => x.Total);
                        rd.SetParameterValue("salesorder", totalOrderSales);
                        var totalOtherSales = salesorders.Where(x => x.Budget == 0).AsEnumerable().Sum(x => x.Total);
                        rd.SetParameterValue("othersales", totalOtherSales);
                        var totalSales = totalOrderSales + totalOtherSales;
                        rd.SetParameterValue("Totalsales", totalSales);
                        if (totalBudget > 0)
                        {
                            var totalAchievements = (totalSales / totalBudget) * 100;
                            rd.SetParameterValue("total_achievements", totalAchievements);
                        }
                        else
                        {
                            var totalAchievements = 100.00;
                            rd.SetParameterValue("total_achievements", totalAchievements);

                        }


                        var filePathOriginal = new Uri(
                        System.IO.Path.GetDirectoryName(
                        System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                        ).LocalPath;//Path of the xml script  

                        filePathOriginal = filePathOriginal + "\\Reports\\pdfReports";

                        try
                        {

                            //limpiamos el directorio
                            System.IO.DirectoryInfo di = new DirectoryInfo(filePathOriginal);

                            foreach (FileInfo file in di.GetFiles())
                            {
                                file.Delete();
                            }
                            foreach (DirectoryInfo dir in di.GetDirectories())
                            {
                                dir.Delete(true);
                            }

                        }
                        catch (Exception e)
                        {
                            var mensaje = e.ToString();
                        }



                        var filename = "Sales Report " + seller.Name + " " + seller.Lastname + " " + DateTime.Now.ToString("dddd").ToUpper() + ".pdf";
                        path = Path.Combine(filePathOriginal, filename);
                        rd.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, path);

                        PdfDocument doc = new PdfDocument();
                        doc.LoadFromFile(path);
                        Image img = doc.SaveAsImage(0);
                        var imagename = "Sales Report " + seller.Name + " " + seller.Lastname + " " + DateTime.Now.ToString("dddd").ToUpper() + ".jpg";
                        pathimage = Path.Combine(filePathOriginal, imagename);
                        img.Save(pathimage);
                        doc.Close();


                        //Para enviar correos
                        try
                        {

                            MailMessage objeto_mail = new MailMessage();
                            SmtpClient client = new SmtpClient();
                            client.Port = 587;
                            client.Host = "smtp-mail.outlook.com";
                            client.Timeout = 100000;
                            client.DeliveryMethod = SmtpDeliveryMethod.Network;
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(Configuration[0], Configuration[1]);

                            objeto_mail.IsBodyHtml = true;
                            objeto_mail.AlternateViews.Add(getEmbeddedImage(pathimage));


                            objeto_mail.From = new MailAddress(Configuration[0]);
                            objeto_mail.To.Add(new MailAddress(seller.Email.ToString()));
                            objeto_mail.Subject = "Sales Report | " + seller.Name + " " + seller.Lastname + " " + DateTime.Now.ToString("dddd").ToUpper();
                            foreach (var bcc in CcData)
                            {
                                MailAddress bccEmail = new MailAddress(bcc.Email.ToString());
                                objeto_mail.CC.Add(bccEmail);
                            }
                            //MailAddress cc = new MailAddress(seller.Supervisor.ToString());  //no se enviara copia porque no se sabe como sacarlo ya que no existe relacion en azure
                            //objeto_mail.CC.Add(cc);

                            //Enviamos el mensaje
                            client.Send(objeto_mail);


                            Console.WriteLine("Email sent successfully to : " + seller.Name + " " + seller.Lastname + " - " + seller.Email.ToString());
                        }

                        catch (Exception e)
                        {
                            Console.WriteLine("Email error, can't sent to : " + seller.Name + " " + seller.Lastname + " - " + seller.Email.ToString() + ". Error: " + e);
                        }


                    }
                    else
                    {
                        Console.WriteLine("No Data was found for " + seller.Name + " " + seller.Lastname);
                    }
                }

            }
            else
            {
                Console.WriteLine("No Data was found...");
                Console.WriteLine("Exit program...");
            }

            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            await Console.Out.WriteLineAsync("Sales Job");
        }
        private AlternateView getEmbeddedImage(String filePath)
        {
            LinkedResource res = new LinkedResource(filePath, MediaTypeNames.Image.Jpeg);
            res.ContentId = Guid.NewGuid().ToString();

            string htmlBody = @"<img src='cid:" + res.ContentId + @"'/>";
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

            alternateView.LinkedResources.Add(res);
            return alternateView;
        }
    }
}